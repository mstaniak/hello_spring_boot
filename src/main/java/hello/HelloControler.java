package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControler {

    @RequestMapping(value = "/")
    public String index(){
        return "Hello world from Spring Boot";
    }

    @RequestMapping(value = "/test")
    public String test(){
        return "Hello world from Spring Boot on test tab";
    }

}
